<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightTicketsTable extends Migration
{
    public function up()
    {
        Schema::create('flight_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plane_id')->unsigned();
            $table->foreign('plane_id')->references('id')->on('planes')->onDelete('cascade');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('flight_tickets');
    }
}
