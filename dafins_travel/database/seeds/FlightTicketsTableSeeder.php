<?php

use Illuminate\Database\Seeder;

class FlightTicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('flight_tickets')->insert([
    		[
        		'plane_id' => '3',
        		'city_id' => '4'
    		],

			[
        		'plane_id' => '1',
        		'city_id' => '2'
    		],

            [
                'plane_id' => '4',
                'city_id' => '1'
            ],

            [
                'plane_id' => '2',
                'city_id' => '3'
            ],
    	]);
    }
}
