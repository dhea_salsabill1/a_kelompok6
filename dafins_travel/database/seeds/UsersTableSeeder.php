<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
        	    'username' => 'Dhea',
            	'password'=>Hash::make("123456"),
                'email' => 'dhea@gmail.com'
    	    ],

			[
        	    'username' => 'Fitri',
            	'password'=>Hash::make("fitri123"),
                'email' => 'fitri@gmail.com'
    	    ],

    	    [
        	    'username' => 'Iga',
            	'password'=>Hash::make("iga123"),
                'email' => 'iga@gmail.com'
    	    ],

    	    [
        	    'username' => 'Melinda',
            	'password'=>Hash::make("melinda"),
                'email' => 'melinda@gmail.com'
    	    ],    	    

            [
                'username' => 'Ilham',
                'password'=>Hash::make("ilham123"),
                'email' => 'ilham@gmail.com'
            ],

            [
                'username' => 'Pujo',
                'password'=>Hash::make("123pujo"),
                'email' => 'pujo@gmail.com'
            ],
        ]);
    }
}
