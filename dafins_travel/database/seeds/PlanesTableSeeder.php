<?php

use Illuminate\Database\Seeder;

class PlanesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planes')->insert([
        	[
        		'airline' => 'Lion Air',
                'avatar' => 'avatars/Lion_Air.png',
        		'class' => 'Business',
    	    	'airfare' => 120000
	    	],

    		[
        		'airline' => 'Batik Air',
                'avatar' => 'avatars/batik_air.png',
        		'class' => 'Business',
        		'airfare' => 150000
    		],

    		[
        		'airline' => 'Garuda Air',
                'avatar' => 'avatars/garuda.png',
        		'class' => 'First',
        		'airfare' => 330000
    		],

            [
                'airline' => 'Sriwijaya Air',
                'avatar' => 'avatars/sriwijaya_air.png',
                'class' => 'Economy',
                'airfare' => 157000
            ],
    	]);
    }
}
