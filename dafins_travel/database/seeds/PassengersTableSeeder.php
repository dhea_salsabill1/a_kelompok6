<?php

use Illuminate\Database\Seeder;

class PassengersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('passengers')->insert([
    		[
        		'user_id' => '2',
        		'name' => 'Fitri',
        		'gender' => 'perempuan',
        		'age' => '20',
        		'phone' => '081239674967'
    		],

			[
        		'user_id' => '3',
        		'name' => 'Iga',
        		'gender' => 'perempuan',
        		'age' => '21',
        		'phone' => '082285749574'
    		],

            [
                'user_id' => '4',
                'name' => 'Melinda',
                'gender' => 'perempuan',
                'age' => '22',
                'phone' => '081384906467'
            ],

            [
                'user_id' => '5',
                'name' => 'Ilham',
                'gender' => 'laki-laki',
                'age' => '21',
                'phone' => '081395375215'
            ],

            [
                'user_id' => '6',
                'name' => 'Azizi Pujo',
                'gender' => 'laki-laki',
                'age' => '19',
                'phone' => '085237584695'
            ],

    	]);
    }
}
