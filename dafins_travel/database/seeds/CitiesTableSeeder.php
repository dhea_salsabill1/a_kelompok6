<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
        	[
        		'origin' => 'Jakarta',
        		'destination' => 'Denpasar'
	    	],

			[
        		'origin' => 'Samarinda',
        		'destination' => 'Jakarta'
	    	],

	    	[
        		'origin' => 'Yogyakarta',
        		'destination' => 'Makassar'
	    	],

	    	[
        		'origin' => 'Bandar Lampung',
        		'destination' => 'Balikpapan'
	    	],    		
    	]);
    }
}
