<?php

use Illuminate\Database\Seeder;

class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->insert([
    		[
        		'flight_ticket_id' => '2',
        		'user_id' => '3',
        		'departure_date' => '2019-05-02',
        		'payment_method' => 'Transfer',
        		'total_ticket' => 1,
        		'total_fare' => 330000
    		],

			[
        		'flight_ticket_id' => '1',
        		'user_id' => '4',
        		'departure_date' => '2019-05-04',
        		'payment_method' => 'Transfer',
        		'total_ticket' => 1,
        		'total_fare' => 150000
    		],

            [
                'flight_ticket_id' => '4',
                'user_id' => '6',
                'departure_date' => '2019-05-10',
                'payment_method' => 'Transfer',
                'total_ticket' => 2,
                'total_fare' => 200000
            ],

            [
                'flight_ticket_id' => '3',
                'user_id' => '5',
                'departure_date' => '2019-05-05',
                'payment_method' => 'Transfer',
                'total_ticket' => 1,
                'total_fare' => 330000
            ],

    	]);
    }
}
