@extends('templates.home')
@section('title')
	Edit user
@endsection
@section('content')
<div class="container" >
	<h3>Form Edit user</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5>{{ $user['username'] }}</h5>
			</div>
		
			<div class="card-body">
				<div class="container text-primary">
			
					<form action="{{ route('users.update',$user['id']) }}" method="POST" class="formgroup"
					enctype="multipart/form-data">
						@csrf
						@method('PUT')
			
			<div class="row">
				<div class="col-md-3">
					<label for="username" class="text-primary">Username</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('username') ? "is-invalid": ""}}" name="username" id="username" value="{{ $user['username'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('username')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="email" class="text-primary">Email</label>
				</div>	
				<div class="col-md-8">
					<input type="email" class="form-control {{$errors->first('email') ? "is-invalid": ""}}" name="email" id="email" value="{{ $user['email'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('email')}}              
                    </div>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-sm-4">
					<button type="submit" class="btn btn-outline-primary" >Update</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection