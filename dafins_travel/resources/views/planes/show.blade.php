@extends('templates.home')
@section('title')
	Detail Plane
@endsection
@section('content')
<h1>Detail Plane </h1>
	<hr>
	<br>
		<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
			<div class="row " style="padding:25px">
				<div class="col-md-2 offset-md-5 offset-sm-4">
					<img src="{{asset('storage/'.$planes['avatar'])}}" style="height:150px; width:150px;" class="rounded-circle" alt="">
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 text-center">
					<h3>{{ $planes['airline'] }}</h3>
				</div>
			</div>
			<hr>
			
			<br>
			
			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Airline
				</div>
					<div class="col-md-4 col-sm-4 ">
						{{ $planes['airline'] }}
					</div>
				</div>

			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Class
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $planes['class'] }}
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Airfare
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $planes['airfare'] }}
				</div>
			</div>			
			<br>

		</div>
			
	</div>
@endsection