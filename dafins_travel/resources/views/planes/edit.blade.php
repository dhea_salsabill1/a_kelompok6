@extends('templates.home')
@section('title')
	Edit Plane
@endsection
@section('content')
<div class="container" >
	<h3>Form Edit Plane</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5>{{ $plane['airline'] }}</h5>
			</div>
		
			<div class="card-body">
				<div class="container text-primary">
			
					<form action="{{ route('planes.update',$plane['id']) }}" method="POST" class="formgroup"
					enctype="multipart/form-data">
						@csrf
						@method('PUT')
			
			<div class="row">
				<div class="col-md-3">
					<label for="airline" class="text-primary">Airline</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('airline') ? "is-invalid": ""}}" name="airline" id="airline" value="{{ $plane['airline'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('airline')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="class" class="text-primary">Class</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('class') ? "is-invalid": ""}}" name="class" id="class" value="{{ $plane['class'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('class')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="airfare" class="text-primary">Airfare</label>
				</div>	
				<div class="col-md-8">
					<input type="number" class="form-control {{$errors->first('airfare') ? "is-invalid": ""}}" name="airfare" id="airfare" value="{{ $plane['airfare'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('airfare')}}              
                    </div>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-sm-4">
					<button type="submit" class="btn btn-outline-primary" >Update</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection