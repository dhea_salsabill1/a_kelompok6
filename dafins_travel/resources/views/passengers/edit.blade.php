@extends('templates.home')
@section('title')
	Edit Passenger
@endsection
@section('content')
<div class="container" >
	<h3>Form Edit Passenger</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5>{{ $passenger['name'] }}</h5>
			</div>
		
			<div class="card-body">
				<div class="container text-primary">
			
					<form action="{{ route('passengers.update',$passenger['id']) }}" method="POST" class="formgroup"
					enctype="multipart/form-data">
						@csrf
						@method('PUT')
			
			<div class="row">
				<div class="col-md-3">
					<label for="user_id" class="text-primary">User Id</label>
				</div>	
				<div class="col-md-8">
					<input type="number" class="form-control {{$errors->first('user_id') ? "is-invalid": ""}}" name="user_id" id="user_id" value="{{ $passenger['user_id'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('user_id')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="name" class="text-primary">Name</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('name') ? "is-invalid": ""}}" name="name" id="name" value="{{ $passenger['name'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('name')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="gender" class="text-primary">Gender</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('gender') ? "is-invalid": ""}}" name="gender" id="gender" value="{{ $passenger['gender'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('gender')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="age" class="text-primary">Age</label>
				</div>	
				<div class="col-md-8">
					<input type="number" class="form-control {{$errors->first('age') ? "is-invalid": ""}}" name="age" id="age" value="{{ $passenger['age'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('age')}}              
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="phone" class="text-primary">Phone</label>
				</div>	
				<div class="col-md-8">
					<input type="number" class="form-control {{$errors->first('phone') ? "is-invalid": ""}}" name="phone" id="phone" value="{{ $passenger['phone'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('phone')}}
                    </div>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-sm-4">
					<button type="submit" class="btn btn-outline-primary" >Update</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection