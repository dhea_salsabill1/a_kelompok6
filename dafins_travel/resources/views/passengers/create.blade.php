@extends('templates.home')
@section('title')
	Create Passenger
@endsection
@section('content')
<div class="container" >
	<h3>Create Passenger</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5> Create a New Passenger</h5>
			</div>
			<div class="card-body">
				<div class="container text-primary">
				<form action="{{ route('accounts.store') }}" class="form-group" method="POST"
					enctype="multipart/form-data">
					@csrf <!-- utk security, supaya gk kena injeksi lainnya utk form nya. -->
					<div class="row" >
						<div class="col-md-3">
							<label for="airline" >Airlane</label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control {{$errors->first('airline') ? "is-invalid": ""}}" name="airline" id="airline" value="{{ old('airline') }}">
							
							<div class="invalid-feedback">
                        		{{$errors->first('airline')}}
                        	</div>

						</div>
						
					</div>
					<br>
					
					<div class="row" >
						<div class="col-md-3">
							<label for="class">Class</label>
						</div>
						
						<div class="col-md-8">
							<input type="text" class="form-control {{$errors->first('class') ? "is-invalid": ""}}" name="class" id="class">

							<div class="invalid-feedback">
                        		{{$errors->first('class')}}
                        	</div>
						</div>
					</div>

					<br>
					
					<div class="row" >
						<div class="col-md-3">
							<label for="airfare">Airfare</label>
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control {{$errors->first('airfare') ? "is-invalid": ""}}" name="airfare" id="airfare">

							<div class="invalid-feedback">
                        		{{$errors->first('airfare')}}
                        	</div>
						</div>
					</div>
					<br>
					
					<div class="row">
						<div class="col-md-3 offset-md-5 offset-sm-4">
							<button type="submit" class="btn btn-outline-primary">Create</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
</div>
@endsection