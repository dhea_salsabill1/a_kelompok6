@extends('templates.home')
@section('title')
	Detail Plane
@endsection
@section('content')
<h1>Detail Plane </h1>
	<hr>
	<br>
		<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
			

			<div class="row">
				<div class="col-md-12 text-center">
					<h3>{{ $passengers['name'] }}</h3>
				</div>
			</div>
			<hr>
			
			<br>
			
			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Gender
				</div>
					<div class="col-md-4 col-sm-4 ">
						{{ $passengers['gender'] }}
					</div>
				</div>

			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Age
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $passengers['age'] }}
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Phone
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $passengers['phone'] }}
				</div>
			</div>			
			<br>

		</div>
			
	</div>
@endsection