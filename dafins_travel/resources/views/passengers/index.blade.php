@extends('templates.home')
@section('title')
	Passenger Index
@endsection
@section('css')

<style>
	body{
		padding-top: 30px;
	}
	th, td {
		padding: 10px;
		text-align: center;
	}
	td a{
		margin: 3px;
		align-content: center;
		color: white;
	}
	td a:hover{
		text-decoration: none;
	}
	td button{
		margin-top: 5px;
		cursor: pointer;
	}
</style>

@endsection
@section('content')

	<div class="container py-4">
		<h3> Passenger List</h3><hr>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-outline-primary " href="{{ route('passengers.create') }}">
					<span data-feather="plus-circle"></span>
					Add<span class="sr-only">(current)</span>
				</a>
			</div>

		</div>
		<br>
		@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr class="table-primary">
						<th scope="col">Id</th>
						<th scope="col">User Id</th>
						<th scope="col">Name</th>
						<th scope="col">Gender</th>
						<th scope="col">Phone</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($passengers as $passenger)
					<tr>
						<td>{{ $passenger['id'] }}</td>
						<td>{{ $passenger['user_id'] }}</td>
						<td>{{ $passenger['name'] }}</td>
						<td>{{ $passenger['gender'] }}</td>
						<td>{{ $passenger['phone'] }}</td>
						<td>
							<a class="btn-sm btn-primary" href="{{ route('passengers.show',$passenger['id']) }}"> <!-- btn-sm = button small , primary = wrn biru -->
							<span data-feather="eye"></span>
							Detail <span class="sr-only">(current)</span></a>
							<a class="btn-sm btn-success d-inline" href="{{route('passengers.edit',$passenger['id']) }}">
							<span data-feather="edit-2"></span>
							Edit <span class="sr-only">(current)</span></a>
	
							<form class="d-inline" onsubmit="return confirm('Delete this account permanently?')" action="{{route('passengers.destroy', $passenger['id'])}}" method="POST">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn-sm btn-danger" value="Delete">
									<span data-feather="trash"></span>
									Delete <span class="sr-only">(current)</span></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="row justify-content-md-center">
            <div class="col-md-auto">
                {{ $passengers->links() }}
            </div>
        </div>
	</div>
@endsection