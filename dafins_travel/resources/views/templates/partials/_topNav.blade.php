<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
	<a class="navbar-brand mx-2" href="#">Dafins Travel</a>
	
	<button class="navbar-toggler" type="button" data-toggle="collapse" datatarget="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" arialabel="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
	</button>
	
	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('users.index') }}">Users</a>
			</li>

			<li class="nav-item">
                    <a class="nav-link" href="{{route('planes.index')}}">Planes</a>
            </li>

            <li class="nav-item">
                    <a class="nav-link" href="{{route('cities.index')}}">Cities</a>
            </li>
		</ul>
		
		<form class="form-inline my-2">
			<input class="form-control mr-sm-2" type="text" placeholder="Search" arialabel="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>
		
		<ul class="navbar-nav px-3">
			<li class="nav item dropdown"> 
				<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->username }}</a> 

				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01"> 
					<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a> 

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
					</form>
				</div>
			</li>
		</ul>
	</div>

</nav>