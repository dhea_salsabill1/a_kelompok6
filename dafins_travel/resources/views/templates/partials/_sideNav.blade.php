<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky py-4">
        <ul class="nav flex-column">
            @if (Auth::user()->role == 'admin')
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('users.show',['id'=>Auth::user()->id]) }}">
                        <span data-feather='users' ></span>
                        Profile
                    </a>
                </li>

                <li class="nav-item "> 
                    <a class="nav-link active" href="{{ route('planes.index') }}">
                        <span data-feather="users"></span>
                        Planes
                    </a>
                </li>                 
            @else
                <li class="nav-item ">
                     <a class="nav-link active" href="{{ route('users.index') }}">
                       <span data-feather="user"></span>
                       Users
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('cities.index') }}">
                        <span data-feather="users"></span>
                        Cities
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('planes.index') }}">
                        <span data-feather="users"></span>
                        Planes
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('passengers.index') }}">
                        <span data-feather="users"></span>
                        Passengers
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('reservations.index') }}">
                        <span data-feather="shopping-cart"></span>
                        Reservations
                    </a>
                </li>
            @endif
        </ul>   
    </div>
</nav>