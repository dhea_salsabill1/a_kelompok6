@extends('templates.home')
@section('title')
	Detail Plane
@endsection
@section('content')
<h1>Detail Plane </h1>
	<hr>
	<br>
		<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">

			<div class="row">
				<div class="col-md-12 text-center">
					<h3>{{ $reservations['id'] }}</h3>
				</div>
			</div>
			<hr>
			
			<br>
			
			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					User Id
				</div>
					<div class="col-md-4 col-sm-4 ">
						{{ $reservations['user_id'] }}
					</div>
				</div>

			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Departure Date
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $reservations['departure_date'] }}
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Payment Method
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $reservations['payment_method'] }}
				</div>
			</div>			
			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Total Ticket
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $reservations['total_ticket'] }}
				</div>
			</div>			
			<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Total Fare
				</div>
				<div class="col-md-4 col-sm-4 ">
					{{ $reservations['total_fare'] }}
				</div>
			</div>			
			<br>

		</div>
			
	</div>
@endsection