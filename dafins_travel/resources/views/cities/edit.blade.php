@extends('templates.home')
@section('title')
	Edit City
@endsection
@section('content')
<div class="container" >
	<h3>Form Edit City</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5>{{ $city['id'] }}</h5>
			</div>
		
			<div class="card-body">
				<div class="container text-primary">
			
					<form action="{{ route('cities.update',$city['id']) }}" method="POST" class="formgroup"
					enctype="multipart/form-data">
						@csrf
						@method('PUT')
			
			<div class="row">
				<div class="col-md-3">
					<label for="origin" class="text-primary">Kota Asal</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('origin') ? "is-invalid": ""}}" name="origin" id="origin" value="{{ $city['origin'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('origin')}} 
                    </div>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3">
					<label for="origin" class="text-primary">Kota Tujuan</label>
				</div>	
				<div class="col-md-8">
					<input type="destination" class="form-control {{$errors->first('destination') ? "is-invalid": ""}}" name="destination" id="destination" value="{{ $city['destination'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('destination')}} 
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3 offset-md-5 offset-sm-4">
					<button type="submit" class="btn btn-outline-primary" >Update</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection