@extends('templates.home')
@section('title')
	Detail City
@endsection
@section('content')
<h1>Detail City </h1>
	<hr>
	<br>
		<div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3>Id City : {{ $cities['id'] }}</h3>
				</div>
			</div>
			<hr>
			
			<br>
			
			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Kota Asal
				</div>
					<div class="col-md-4 col-sm-4 ">
						{{ $cities['origin'] }}
					</div>
			</div>

				<br>

			<div class="row">
				<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
					Kota Tujuan
				</div>
					<div class="col-md-4 col-sm-4 ">
						{{ $cities['destination'] }}
					</div>
				</div>
			</div>
		<br>
@endsection