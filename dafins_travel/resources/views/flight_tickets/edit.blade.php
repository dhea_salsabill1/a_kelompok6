@extends('templates.home')
@section('title')
	Edit Flight Ticket
@endsection
@section('content')
<div class="container" >
	<h3>Form Edit Flight Ticket</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5>{{ $flight_ticket['id'] }}</h5>
			</div>
		
			<div class="card-body">
				<div class="container text-primary">
			
					<form action="{{ route('flight_tickets.update',$flight_ticket['id']) }}" method="POST" class="formgroup"
					enctype="multipart/form-data">
						@csrf
						@method('PUT')
			
			<div class="row">
				<div class="col-md-3">
					<label for="user_id" class="text-primary">User Id</label>
				</div>	
				<div class="col-md-8">
					<input type="text" class="form-control {{$errors->first('user_id') ? "is-invalid": ""}}" name="user_id" id="user_id" value="{{ $flight_ticket['user_id'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('user_id')}}
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="plane_id" class="text-primary">Plane Id</label>
				</div>	
				<div class="col-md-8">
					<input type="number" class="form-control {{$errors->first('plane_id') ? "is-invalid": ""}}" name="plane_id" id="plane_id" value="{{ $flight_ticket['plane_id'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('plane_id')}}
                    </div>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-3">
					<label for="city_id" class="text-primary">City Id</label>
				</div>	
				<div class="col-md-8">
					<input type="number" class="form-control {{$errors->first('city_id') ? "is-invalid": ""}}" name="city_id" id="city_id" value="{{ $flight_ticket['city_id'] }}">

					<div class="invalid-feedback">
                        	{{$errors->first('city_id')}}              
                    </div>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-sm-4">
					<button type="submit" class="btn btn-outline-primary" >Update</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection