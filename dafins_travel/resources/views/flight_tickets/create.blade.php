@extends('templates.home')
@section('title')
	Create Plane
@endsection
@section('content')
<div class="container" >
	<h3>Create Plane</h3>
	<hr>
	@if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">          
                <strong>{{ session('status') }}</strong>       
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">       
                    <span aria-hidden="true">&times;</span>  
                </button>             
            </div>         
        @endif
		<div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
			<div class="card-header bg-primary text-white">
				<h5> Create a New Plane</h5>
			</div>
			<div class="card-body">
				<div class="container text-primary">
				<form action="{{ route('flight_tickets.store') }}" class="form-group" method="POST"
					enctype="multipart/form-data">
					@csrf <!-- utk security, supaya gk kena injeksi lainnya utk form nya. -->
					
					<div class="row" >
						<div class="col-md-3">
							<label for="plane_id">Plane Id</label>
						</div>
						
						<div class="col-md-8">
							<input type="number" class="form-control {{$errors->first('plane_id') ? "is-invalid": ""}}" name="plane_id" id="plane_id">

							<div class="invalid-feedback">
                        		{{$errors->first('plane_id')}}
                        	</div>
						</div>
					</div>

					<br>
					
					<div class="row" >
						<div class="col-md-3">
							<label for="city_id">City Id</label>
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control {{$errors->first('city_id') ? "is-invalid": ""}}" name="city_id" id="city_id">

							<div class="invalid-feedback">
                        		{{$errors->first('city_id')}}
                        	</div>
						</div>
					</div>
					<br>
					
					<div class="row">
						<div class="col-md-3 offset-md-5 offset-sm-4">
							<button type="submit" class="btn btn-outline-primary">Create</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
</div>
@endsection