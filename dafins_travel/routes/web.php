<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dafins-travel', function(){
	return view('home');
});


Route::middleware(['auth','role'])->group(function(){
	Route::resource('users','UserController');
	Route::resource('planes','PlaneController');
});


Route::middleware(['auth'])->group(function () { 
	Route::get('users/{user}', 'UserController@show')->name('users.show'); 
	Route::get('planes', 'PlaneController@index')->name('planes.index'); 
	Route::get('/planes/{plane}','PlaneController@show')->name('planes.show'); 
});


Route::prefix('users')->group(function (){
	Route::get('/','UserController@index') -> name ('users.index');
	Route::get('/create','UserController@create') -> name ('users.create');
	Route::get('/{id}/edit','UserController@edit') -> name ('users.edit');
	Route::get('/{id}','UserController@show') -> name ('users.show');
	Route::post('/','UserController@store') -> name ('users.store');
	Route::put('/{id}','UserController@update') -> name ('users.update');
	Route::delete('/{id}','UserController@destroy') -> name ('users.destroy');
});

Route::prefix('cities')->group(function()
{
	Route::get('/','CityController@index') -> name ('cities.index');
	Route::get('/create','CityController@create') -> name ('cities.create');
	Route::get('/{id}/edit','CityController@edit') -> name ('cities.edit');
	Route::get('/{id}','CityController@show') -> name ('cities.show');
	Route::post('/','CityController@store') -> name ('cities.store');
	Route::put('/{id}','CityController@update') -> name ('cities.update');
	Route::delete('/{id}','CityController@destroy') -> name ('cities.destroy');
});

Route::prefix('planes')->group(function()
{
	Route::get('/','PlaneController@index') -> name ('planes.index');
	Route::get('/create','PlaneController@create') -> name ('planes.create');
	Route::get('/{id}/edit','PlaneController@edit') -> name ('planes.edit');
	Route::get('/{id}','PlaneController@show') -> name ('planes.show');
	Route::post('/','PlaneController@store') -> name ('planes.store');
	Route::put('/{id}','PlaneController@update') -> name ('planes.update');
	Route::delete('/{id}','PlaneController@destroy') -> name ('planes.destroy');
});

Route::prefix('flight_tickets')->group(function()
{
	// Route::get('/','FlightTicketController@index') -> name ('flight_tickets.index');
	// Route::get('/create','PlaneController@create') -> name ('planes.create');
	Route::get('/create','FlightTicketController@create') -> name ('flight_tickets.create');
	// Route::get('/{id}/edit','PlaneController@edit') -> name ('planes.edit');
	// Route::get('/{id}','PlaneController@show') -> name ('planes.show');
	// Route::post('/','PlaneController@store') -> name ('planes.store');
	Route::post('/','FlightTicketController@store') -> name ('flight_tickets.store');
	// Route::put('/{id}','PlaneController@update') -> name ('planes.update');
	// Route::delete('/{id}','PlaneController@destroy') -> name ('planes.destroy');
});

Route::prefix('passengers')->group(function()
{
	Route::get('/','PassengerController@index') -> name ('passengers.index');
	Route::get('/create','PassengerController@create') -> name ('passengers.create');
	Route::get('/{id}/edit','PassengerController@edit') -> name ('passengers.edit');
	Route::get('/{id}','PassengerController@show') -> name ('passengers.show');
	Route::post('/','PassengerController@store') -> name ('passengers.store');
	Route::put('/{id}','PassengerController@update') -> name ('passengers.update');
	Route::delete('/{id}','PassengerController@destroy') -> name ('passengers.destroy');
});

Route::prefix('reservations')->group(function()
{
	Route::get('/','ReservationController@index') -> name ('reservations.index');
	Route::get('/create','ReservationController@create') -> name ('reservations.create');
	Route::get('/{id}/edit','ReservationController@edit') -> name ('reservations.edit');
	Route::get('/{id}','ReservationController@show') -> name ('reservations.show');
	Route::post('/','ReservationController@store') -> name ('reservations.store');
	Route::put('/{id}','ReservationController@update') -> name ('reservations.update');
	Route::delete('/{id}','ReservationController@destroy') -> name ('reservations.destroy');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout','Auth\LoginController@logout');