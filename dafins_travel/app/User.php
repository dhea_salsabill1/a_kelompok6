<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Passenger;
use App\Reservation;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'username', 'email', 'password',
    ];

    protected $hidden = [
        'password','remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function passenger(){
        return $this->belongsTo(Passenger::class,'user_id','id');
    }

    public function reservation(){
        return $this->hasMany(Reservation::class,'user_id','id');
    }
}
