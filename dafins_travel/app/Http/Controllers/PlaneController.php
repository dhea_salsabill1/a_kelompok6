<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Plane;

class PlaneController extends Controller
{
    public function index()
    {
        $planes=Plane::paginate(5);
        return view('planes.index',compact('planes'));
    }


    public function show($id)
    {
        $plane = Plane::findOrFail($id);
        return view('planes.show',['planes' => $plane]);
    }

    public function create()
    {
        return view('planes.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'airline'=>'required',
            'avatar'=>'required|mimes:jpg,jpeg,png,bmp', 
            'class'=>'required',
            'airfare'=>'required'   
        ]);
        
        $new_plane = new Plane();         
        $new_plane->airline = $request->get('airline');

        if ($request->file('avatar')) {             
            $file = $request->file('avatar')->store('avatars','public');
            $new_plane->avatar = $file;         
        }
        
        $new_plane->class = $request->get('class');
        $new_plane->airfare = $request->get('airfare');
        
        $new_plane->save();         
        return redirect()->route('planes.create')->with('status','Plane Succesfully Created');
    }

    public function create_ticket()
    {
        return view('flight_tickets.create');
    }

    public function store_ticket(Request $request)
    {
        $request->validate([
            'plane_id'=>'required',
            'city_id'=>'required'
        ]);
        
        $new_flight_ticket = new Flight_Ticket();         
        $new_flight_ticket->plane_id = $request->get('plane_id');
        $new_flight_ticket->city_id = $request->get('city_id');
        
        $new_flight_ticket->save();         
        return redirect()->route('flight_tickets.create')->with('status','Flight Ticket Succesfully Created');
    }

    public function edit($id)
    {
        $plane = Plane::findOrFail($id);
        return view('planes.edit',['plane' => $plane]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'class'=>'required',
            'airfare'=>'required'
        ]);  

        $update_plane = Plane::findOrFail($id); 
        $update_plane->class = $request->get('class'); 
        $update_plane->airfare = $request->get('airfare'); 
        
            $update_plane->save(); 
            return redirect()->route('planes.edit',['id'=>$id])->with('status','Plane succesfully updated'); 
    }

    public function destroy($id)
    {
        $plane = Plane::findOrFail($id); 

        $plane->delete(); 
        return redirect()->route('planes.index')->with('status', 'Plane Successfully deleted'); 
    }
}
