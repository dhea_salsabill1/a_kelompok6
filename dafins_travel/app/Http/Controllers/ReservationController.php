<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;

class ReservationController extends Controller
{
    public function index()
    {
        $reservations=Reservation::paginate(5);
        return view('reservations.index',compact('reservations'));
    }


    public function show($id)
    {
        $reservation = Reservation::findOrFail($id);
        return view('reservations.show',['reservations' => $reservation]);
    }

    public function create()
    {
        return view('reservations.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'gender'=>'required',
            'age'=>'required',
            'phone'=>'required'   
        ]);
        
        $new_reservation = new Reservations();         
        $new_reservation->name = $request->get('name');
        $new_reservation->gender = $request->get('gender');
        $new_reservation->age = $request->get('age');
        $new_reservation->phone = $request->get('phone');
        
        $new_reservation->save();         
        return redirect()->route('reservations.create')->with('status','reservation Succesfully Created');
    }

    public function edit($id)
    {
        $reservation = Reservation::findOrFail($id);
        return view('reservations.edit',['reservation' => $reservation]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'name'=>'required',
            'gender'=>'required',
            'age'=>'required',
            'phone'=>'required'   
        ]);  

        $update_reservation = Reservation::findOrFail($id); 
        $update_reservation->name = $request->get('name'); 
        $update_reservation->gender = $request->get('gender'); 
        $update_reservation->age = $request->get('age'); 
        $update_reservation->phone = $request->get('phone'); 
        
        $update_reservation->save(); 
            return redirect()->route('reservations.edit',['id'=>$id])->with('status','reservation succesfully updated'); 
    }

    public function destroy($id)
    {
        $reservation = Reservation::findOrFail($id); 

        $reservation->delete(); 
        return redirect()->route('reservations.index')->with('status', 'reservation Successfully deleted'); 
    }
}
