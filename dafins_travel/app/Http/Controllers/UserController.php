<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users=User::paginate(5);
        return view('users.index',compact('users'));
    }


    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show',['users' => $user]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'username'=>'required|min:4|max:100|unique:users',             
            'password'=>'required',
            'password_confirmation'=>'required|same:password'   
        ]);
        
        $new_user = new User();         
        $new_user->username = $request->get('username');
        $new_user->password = Hash::make($request->get('password'));
        
        $new_user->save();         
        return redirect()->route('users.create')->with('status','User Succesfully Created');

    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit',['user' => $user]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'username'=>'required|max:200'
        ]);  

        $update_user = User::findOrFail($id); 
        $update_user->username = $request->get('username'); 
        
            $update_user->save(); 
            return redirect()->route('users.edit',['id'=>$id])->with('status','user succesfully updated'); 
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id); 

        $user->delete(); 
        return redirect()->route('users.index')->with('status', 'user Successfully deleted'); 
    }
}
