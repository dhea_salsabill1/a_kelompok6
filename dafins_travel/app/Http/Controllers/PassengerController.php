<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Passenger;

class PassengerController extends Controller
{
    public function index()
    {
        $passengers=Passenger::paginate(5);
        return view('passengers.index',compact('passengers'));
    }


    public function show($id)
    {
        $passenger = Passenger::findOrFail($id);
        return view('passengers.show',['passengers' => $passenger]);
    }

    public function create()
    {
        return view('passengers.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'gender'=>'required',
            'age'=>'required',
            'phone'=>'required'   
        ]);
        
        $new_passenger = new Passengers();         
        $new_passenger->name = $request->get('name');
        $new_passenger->gender = $request->get('gender');
        $new_passenger->age = $request->get('age');
        $new_passenger->phone = $request->get('phone');
        
        $new_passenger->save();         
        return redirect()->route('passengers.create')->with('status','Passenger Succesfully Created');
    }

    public function edit($id)
    {
        $passenger = Passenger::findOrFail($id);
        return view('passengers.edit',['passenger' => $passenger]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'name'=>'required',
            'gender'=>'required',
            'age'=>'required',
            'phone'=>'required'   
        ]);  

        $update_passenger = Passenger::findOrFail($id); 
        $update_passenger->name = $request->get('name'); 
        $update_passenger->gender = $request->get('gender'); 
        $update_passenger->age = $request->get('age'); 
        $update_passenger->phone = $request->get('phone'); 
        
        $update_passenger->save(); 
            return redirect()->route('passengers.edit',['id'=>$id])->with('status','Passenger succesfully updated'); 
    }

    public function destroy($id)
    {
        $passenger = Passenger::findOrFail($id); 

        $passenger->delete(); 
        return redirect()->route('passengers.index')->with('status', 'Passenger Successfully deleted'); 
    }
}
