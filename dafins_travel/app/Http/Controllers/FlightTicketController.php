<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flight_Ticket;

class FlightTicketController extends Controller
{
    public function index()
    {
        $flight_tickets = Flight_Ticket::paginate(5);
        return view('flight_tickets.index',compact('flight_tickets'));
    }


    public function show($id)
    {
        $flight_ticket = Flight_Ticket::findOrFail($id);
        return view('flight_tickets.show',['flight_tickets' => $flight_ticket]);
    }

    public function create()
    {
        return view('flight_tickets.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'plane_id'=>'required',
            'city_id'=>'required'
        ]);
        
        $new_flight_ticket = new Flight_Ticket();         
        $new_flight_ticket->plane_id = $request->get('plane_id');
        $new_flight_ticket->city_id = $request->get('city_id');
        
        $new_flight_ticket->save();         
        return redirect()->route('flight_tickets.create')->with('status','Flight Ticket Succesfully Created');

    }

    public function edit($id)
    {
        $flight_ticket = Flight_Ticket::findOrFail($id);
        return view('flight_tickets.edit',['flight_ticket' => $flight_ticket]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'plane_id'=>'required',
            'city_id'=>'required'
        ]);  

        $update_flight_ticket = Flight_Ticket::findOrFail($id); 
        $update_flight_ticket->plane_id = $request->get('plane_id'); 
        $update_flight_ticket->city_id = $request->get('city_id'); 
        
            $update_flight_ticket->save(); 
            return redirect()->route('flight_tickets.edit',['id'=>$id])->with('status','Flight Ticket succesfully updated'); 
    }

    public function destroy($id)
    {
        $flight_ticket = Flight_Ticket::findOrFail($id); 

        $flight_ticket->delete(); 
        return redirect()->route('flight_tickets.index')->with('status', 'Flight Ticket Successfully deleted'); 
    }
}

