<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public function index()
    {
        $cities=City::paginate(5);
        return view('cities.index',compact('cities'));
    }


    public function show($id)
    {
        $city = City::findOrFail($id);
        return view('cities.show',['cities' => $city]);
    }

    public function create()
    {
        return view('cities.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'origin'=>'required',
            'destination'=>'required'
        ]);
        
        $new_city = new City();         
        $new_city->origin = $request->get('origin');
        $new_city->destination = $request->get('destination');
        
        $new_city->save();         
        return redirect()->route('cities.create')->with('status','City Succesfully Created');

    }

    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view('cities.edit',['city' => $city]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'origin'=>'required',
            'destination'=>'required'
        ]);  

        $update_city = City::findOrFail($id); 
        $update_city->origin = $request->get('origin'); 
        $update_city->destination = $request->get('destination'); 
        
            $update_city->save(); 
            return redirect()->route('cities.edit',['id'=>$id])->with('status','City succesfully updated'); 
    }

    public function destroy($id)
    {
        $city = City::findOrFail($id); 

        $city->delete(); 
        return redirect()->route('cities.index')->with('status', 'City Successfully deleted'); 
    }
}
