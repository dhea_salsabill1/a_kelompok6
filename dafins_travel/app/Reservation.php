<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Flight_Ticket;

class Reservation extends Model
{
    protected $fillable=[
    	'flight_ticket_id','user_id','total_fare',
    ];

    public function user(){
    	return $this->hasMany(User::class,'user_id','id');
    }
    
    public function flight_ticket(){
    	return $this->hasMany(Flight_Ticket::class,'flight_ticket_id','id');
    }
}
