<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Plane;

class City extends Model
{
    protected $fillable=[
    	'origin','destination',
    ];

    public function plane(){
    	return $this->belongTo(Plane::class,'plane_id','id');
    }
}
