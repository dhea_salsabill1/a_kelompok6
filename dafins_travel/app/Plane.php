<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;

class Plane extends Model
{
    protected $fillable=[
    	'airline','class','airfare',
    ];

    public function city(){
    	return $this->belongTo(City::class,'city_id','id');
    }
}
