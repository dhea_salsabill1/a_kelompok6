<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Passenger extends Model
{
    protected $fillable=[
    	'name','age','phone',
    ];

    public function user(){
    	return $this->hasOne(User::class,'user_id','id');
    }
}
